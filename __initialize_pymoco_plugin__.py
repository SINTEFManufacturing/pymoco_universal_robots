# coding=utf-8

"""
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2014-2021"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


from .robots import register_robot_type
from .robots.ur10 import UR10
from .robots.ur5 import UR5
from .robots.ur5_ml_cyl_rolls import UR5_ML_CylRolls
from .robots.ur3_dh import UR3_DH
from .robots.ur5_dh import UR5_DH
from .robots.ur10_dh import UR10_DH
from .robots.ur3e_dh import UR3e_DH
from .robots.ur5e_dh import UR5e_DH
from .robots.ur10e_dh import UR10e_DH
from .robots.ur16e_dh import UR16e_DH
from .facades import register_facade_type
from .facades.ur_udp_legacy import URUDPLegacyFacade
from .facades.ur_tcp_legacy import URTCPLegacyFacade
from .facades.ur_rtde import URRTDEFacade


for rt in (UR5, UR5_DH, UR5_ML_CylRolls, UR10, UR10_DH, UR3_DH,
           UR3e_DH, UR5e_DH, UR10e_DH, UR16e_DH):
    register_robot_type(rt)

for ft in (URRTDEFacade, URUDPLegacyFacade, URTCPLegacyFacade):
    register_facade_type(ft)
