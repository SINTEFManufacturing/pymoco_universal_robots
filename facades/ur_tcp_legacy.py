# coding=utf-8
"""
Module for a legacy facade to a TCP-based "router" on the Universal
Robot controller platform.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2011-2021"
__credits__ = ["Morten Lind", "Johannes Schrimpf"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import socket
import logging

import pymoco.kinematics
import pymoco.robots
from ..input.ur_gw_publisher import URGwPublisher
from ..control.facilities import URGwControlFacility

from .legacy_facade import LegacyFacade


class URTCPLegacyFacade(LegacyFacade):
    """Facade for the UR robot facading the legacy LLCPublisher and
    BaseController facilities."""

    STANDARD_UR_TCP_PORT = 5002

    def __init__(self, **kwargs):
        LegacyFacade.__init__(self, **kwargs)
        self.cycle_number = 0
        self._rob_def = pymoco.robots.get_robot_type(kwargs['rob_type'])()
        self._frame_computer = pymoco.kinematics.FrameComputer(
            rob_def=self._rob_def)
        self._gw_host = kwargs.get('rob_host')
        self._gw_port = kwargs['rob_port'] = kwargs.get('rob_port', self.STANDARD_UR_TCP_PORT)
        self._log = logging.getLogger(self.__class__.__name__ +
                                      '({}:{})'.format(self._gw_host, self._gw_port))
        self._log.setLevel(kwargs.get('log_level', logging.INFO))

        # The port to the UR-controller should not be over-riden
        # except under special circumstances
        if self._gw_port != self.STANDARD_UR_TCP_PORT:
            self._log.warn('The port given ({:d}) is different ' +
                      'from the standard UR TCP router port ({:d})'
                      .format(self._gw_port, self.STANDARD_UR_TCP_PORT), 3)
        self._gw_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._gw_socket.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
        # self._gw_socket.settimeout(1.0)
        # self._gw_out_socket.connect((self._gw_host, self._gw_out_port))
        self._llcp = URGwPublisher(robot_facade=self,
                                   gw_socket=self._gw_socket,
                                   **kwargs)
        # , bind_host=self._bind_host)
        if self._gw_host is None:
            self._gw_host = self._llcp.llc_host
        self._control = URGwControlFacility(robot_facade=self,
                                            gw_socket=self._gw_socket,
                                            **kwargs)
        # gw_in_port=self._gw_in_port,
        # gw_host=self._gw_host)

    def start(self):
        self._log.warn('Connecting to robot')
        self._gw_socket.connect((self._gw_host, self._gw_port))
        self._llcp.start()
        self._control.initialize()

    def stop(self, join=False):
        self._log.warn('Stopping robot facade.')
        del self._control
        self._llcp.stop()
        if join:
            self._llcp.join()
        del self._llcp
