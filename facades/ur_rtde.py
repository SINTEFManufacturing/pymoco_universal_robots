# coding=utf-8

"""
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2021"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import time
import threading

import numpy as np
import math3d as m3d

from . import RobotFacade
from .. import robots
from .. import kinematics


class URRTDEFacade(RobotFacade, threading.Thread):
    """A PyMoCo facade to the Universal Robots controllers using the
    Python RTDE interface 'ur_rtde'.
    Code repository: https://gitlab.com/sdurobotics/ur_rtde.
    Documentation: https://sdurobotics.gitlab.io/ur_rtde.
    """

    class Error(Exception):
        pass

    def __init__(self, **kwargs):
        # Delayed load of ur_rtde:
        import rtde_control
        import rtde_receive
        RobotFacade.__init__(self, **kwargs)
        threading.Thread.__init__(self)
        self.daemon = True
        self._rob_def = robots.get_robot_type(kwargs['rob_type'])()
        self._frame_computer = kinematics.FrameComputer(
            rob_def=self._rob_def)
        self._host = kwargs.get('rob_host', '127.0.0.1')
        # self._port = kwargs.get('rob_port',  3003)
        self._cycle_time_nom = kwargs.get('cycle_time', 0.01)
        self._cycle_time = self._cycle_time_nom
        self._rtde_ctrl = rtde_control.RTDEControlInterface(self._host)
        self._rtde_recv = rtde_receive.RTDEReceiveInterface(self._host)
        self._sj_la = 0.03  # servoJ lookahead_time [0.03; 0.2]
        self._sj_gain = 2000  # servoJ gain [100; 2000]
        self._sj_time = 0.95 * self._cycle_time_nom
        self.__stop = False

    def set_cmd_joint_pos(self, joint_pos_cmd):
        """Set the commanded joint positions."""
        self._rtde_ctrl.servoJ(joint_pos_cmd, 0.0, 0.0,
                               self._sj_time, self._sj_la, self._sj_gain)
        self._q_increment = joint_pos_cmd - self._q_tracked
        self._q_tracked = joint_pos_cmd.copy()
        self._frame_computer.joint_angles_vec = self._q_tracked
        with self._control_cond:
            self._control_cond.notify_all()

    cmd_joint_pos = property(RobotFacade.get_cmd_joint_pos, set_cmd_joint_pos)

    def get_UR_flange_pose(self):
        return m3d.Transform(m3d.PoseVector(
            self._rtde_recv.getActualTCPPose()))

    def stop(self, join=False):
        """Stop the interaction with the robot system."""
        self.__stop = True
        if join:
            self.join()

    def run(self):
        self._q_increment = np.zeros(self._rob_def.dof)
        self._q_actual = np.array(self._rtde_recv.getActualQ())
        self._q_tracked = self._q_actual.copy()
        self._q_error = self._q_tracked - self._q_actual
        if self._q_tracked is None:
            raise self.__class__.Error(
                '.__init__: Failed to read actual joint positions!')
        self._frame_computer.joint_angles_vec = self._q_tracked
        while not self.__stop:
            t_start = time.time()
            self._q_actual = np.array(self._rtde_recv.getActualQ())
            self._t_packet = time.time()
            self._q_error = self._q_tracked - self._q_actual
            qerr_norm = np.linalg.norm(self._q_error)
            self._log.debug(f'Joint position tracking error: {qerr_norm}')
            if qerr_norm > self._tracking_tol:
                self.cmd_joint_pos = self._q_actual
                self._disconnect()
                raise self.Error(
                    f'Tracking error ({np.linalg.norm(self._q_error)}) ' +
                    f'exceeding tolerance ({self._tracking_tol})!')
            self._event_publisher.publish(None)
            t_rem = self._cycle_time_nom - (time.time()-t_start)
            if t_rem > 0:
                time.sleep(t_rem)
            else:
                self._log.warn(
                    f'Cycle time ({self._cycle_time_nom - t_rem}s) ' +
                    'was longer than nominal cycle time ' +
                    f'({self._cycle_time_nom}s)!')
