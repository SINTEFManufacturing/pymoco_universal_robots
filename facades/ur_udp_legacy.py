# coding=utf-8
"""
Module for a legacy facade to a TCP-based 'router' on the
Universal Robot controller platform.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2011-2021"
__credits__ = ["Morten Lind", "Johannes Schrimpf"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import socket

import pymoco.kinematics
import pymoco.robots
from pymoco.input.ur_gw_publisher import URGwPublisher
from pymoco.control.facilities import URGwControlFacility

from .legacy_facade import LegacyFacade


class URUDPLegacyFacade(LegacyFacade):
    """Facade for the UR robot facading the legacy LLCPublisher and
    BaseController facilities."""
    def __init__(self, **kwargs):
        self._log_level = kwargs.get('log_level', 2)
        LegacyFacade.__init__(self, **kwargs)
        self.cycle_number = None
        self._rob_def = pymoco.robots.get_robot_type(kwargs['rob_type'])()
        # self._q_zero_offsets = kwargs.get('q_zero_offsets',
        #     self._rob_def.q_zero_offsets)
        self._frame_computer = pymoco.kinematics.FrameComputer(
            rob_def=self._rob_def)
        self._gw_host = kwargs.get('rob_host')
        self._bind_host = kwargs.get('bind_host', '127.0.0.1')
        self._gw_in_port = kwargs.get('rob_in_port',
                                      pymoco.control.llc_in_port)
        kwargs['rob_in_port'] = self._gw_in_port
        self._gw_in_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self._gw_out_port = kwargs.get('rob_out_port',
                                       pymoco.input.llc_out_port)
        kwargs['rob_out_port'] = self._gw_out_port
        self._gw_out_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        # self._gw_out_socket.connect((self._gw_host, self._gw_out_port))
        self._llcp = URGwPublisher(robot_facade=self,
                                   gw_socket=self._gw_out_socket,
                                   **kwargs)
        # , bind_host=self._bind_host)
        self._control = URGwControlFacility(robot_facade=self,
                                            gw_socket=self._gw_in_socket,
                                            **kwargs)
        # gw_in_port=self._gw_in_port,
        # gw_host=self._gw_host)

    def start(self):
        self._gw_in_socket.connect((self._gw_host, self._gw_in_port))
        self._gw_out_socket.settimeout(1.0)
        self._gw_out_socket.bind((self._bind_host, self._gw_out_port))
        self._llcp.start()
        self._control.initialize()

    def stop(self, join=False):
        self._log.info('Stopping robot facade.')
        del self._control
        self._llcp.stop()
        if join:
            self._llcp.join()
        del self._llcp
